package br.com.fdvtp1.entities;
import br.com.fdvtp1.utility.*;

public class Student {
	
	private String name;
	private double scoreV1;
	private double scoreV2;
	private double averageScore;
	private StudentStatus studentStatus; 
	
	public Student(){}
	
	public Student(String name, Double scoreV1, Double scoreV2){
		this.name = name;
		this.scoreV1 = scoreV1;
		this.scoreV2 = scoreV2;
	}
	
	public String getName(){
		return this.name;
	}
	public void setName(String name){
		this.name = name;
	}
	public double getScoreV1(){
		return this.scoreV1;
	}
	public void setScoreV1(double score){
		this.scoreV1 = score;
	}
	public Double getScoreV2(){
		return this.scoreV2;
	}
	public void setScoreV2(double score){
		this.scoreV2 = score;
	}
	public Double getAverageScore(){
		return this.averageScore;
	}
	public void setAverageScore(double averageScore){
		this.averageScore = averageScore;
	}
	
	public StudentStatus getStudentStatus(){
		return this.studentStatus;
	}
	public void setStudentStatus(StudentStatus studentStatus){
		this.studentStatus = studentStatus;
	}	
}
