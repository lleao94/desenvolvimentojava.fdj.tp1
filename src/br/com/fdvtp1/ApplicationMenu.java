package br.com.fdvtp1;

import java.util.Scanner;

import br.com.fdvtp1.controller.*;
import br.com.fdvtp1.repository.*;
import br.com.fdvtp1.utility.PrintObject;
import br.com.fdvtp1.entities.*;

public class ApplicationMenu {

	private Scanner scanner;
	private DataRepository dataRepository;
	private StudentController studentController;
	private SchoolClassController schoolClassController;
	public ApplicationMenu(){
		scanner = new Scanner(System.in);
		
		//Create Repository data of School
		dataRepository = new DataRepository(100);
		
		//Create Controllers
		studentController = new StudentController(this.dataRepository);
		schoolClassController = new SchoolClassController(this.dataRepository);
	}
		
	public void startMenu(){
		int option = 0;
		do{
			System.out.println("\n------------------------------------");
			System.out.println("1.Register scores of a new student.");
			System.out.println("2.Consult scores of a student.");
			System.out.println("3.Consult scores of School class.");
			System.out.println("0.Exit.");
			System.out.println("------------------------------------");
			
			System.out.println("Write an option: ");
			option = scanner.nextInt();
		
			switch (option) {
			case 1:	
				System.out.println("----------- Register ---------------");
				
				Student student = new Student();
				
				System.out.println("Student Name: ");
				student.setName(scanner.next());
				
				System.out.println("Score of V1: ");
				student.setScoreV1(scanner.nextDouble());
				
				System.out.println("Score of V2: ");
				student.setScoreV2(scanner.nextDouble());
				
				this.studentController.register(student);
				
				break;
			case 2:
				System.out.println("--------- Consult Student ---------");
				
				System.out.println("Registry number:");
				int id = scanner.nextInt();
				
				PrintObject.printStudent(this.studentController.studentConsultation(id));
				
				break;
			case 3:
				System.out.println("--------- Consult Class ---------");
				
				String[] students = this.schoolClassController.schoolClassConsultation();
				
				for (int i = 0; i < students.length; i++) {
					if(students[i] != null){
						PrintObject.printStudent(this.studentController.studentConsultation(i));
					}
				}
				
				break;
			case 0:
				
				System.out.println("Finished Program.");
				System.exit(0);
				break;
			default:
				
				System.out.println("Option invalidates try again. \n ");
				break;
			}
		}while(option != 0);
	}

}
