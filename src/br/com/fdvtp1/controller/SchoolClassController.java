package br.com.fdvtp1.controller;

import br.com.fdvtp1.repository.DataRepository;

public class SchoolClassController {
	private DataRepository dataRepository;
	
	public SchoolClassController(DataRepository dataRepository){
		this.dataRepository = dataRepository;
	};
	
	public String[] schoolClassConsultation (){
		return this.dataRepository.getStudentNames();
	}
}
