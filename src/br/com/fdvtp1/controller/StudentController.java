package br.com.fdvtp1.controller;

import br.com.fdvtp1.entities.*;
import br.com.fdvtp1.repository.DataRepository;
import br.com.fdvtp1.utility.StudentStatus;

public class StudentController {

	private DataRepository dataRepository;
	
	public StudentController(DataRepository dataRepository){
		this.dataRepository = dataRepository;
	};
	
	public void register(Student student){
		try{
			
			this.dataRepository.registerStudentName(student.getName());
			int id = this.dataRepository.getIdByStudent(student.getName());
			this.dataRepository.setScorev1(id, student.getScoreV1());
			this.dataRepository.setScorev2(id, student.getScoreV2());
			
			System.out.println("Registry done! Registry number of student is : " + id);
			
		}catch(Exception ex){
			System.out.println(ex.toString());
		}
	};	

	public Student studentConsultation(int id){
		
		Student student = new Student();
		student.setName(this.dataRepository.getName(id));
		student.setScoreV1(this.dataRepository.getScorev1(id));
		student.setScoreV2(this.dataRepository.getScorev2(id));
		this.calculateAverage(student);
		
		return student;
	}
	
	private void calculateAverage(Student student){
		double averageScore = (student.getScoreV1() + student.getScoreV2()) / 2;
		student.setAverageScore(averageScore);
	
		if(averageScore < StudentStatus.Disapproved.getValor() ){
			student.setStudentStatus(StudentStatus.Disapproved);	
		}else if (averageScore < StudentStatus.FinalTest.getValor()){
			student.setStudentStatus(StudentStatus.FinalTest);
		}else{
			student.setStudentStatus(StudentStatus.Approved);
		}
	}
}
