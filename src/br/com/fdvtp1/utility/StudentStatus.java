package br.com.fdvtp1.utility;

public enum StudentStatus {
	
	Disapproved(4),FinalTest(7),Approved(10);

	private final int valorEnum;
	
	StudentStatus(int valor){
		valorEnum = valor; 
	}
	public int getValor(){
		return valorEnum;
	}
	
}
