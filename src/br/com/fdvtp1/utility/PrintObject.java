package br.com.fdvtp1.utility;

import br.com.fdvtp1.entities.Student;

public class PrintObject {

	public static void printStudent(Student student){
		System.out.println("\nStudent Name: " + student.getName());
		System.out.println("Score v1: " + student.getScoreV1());
		System.out.println("Score v2: " + student.getScoreV2());
		System.out.println("Average Score: " + student.getAverageScore());
		System.out.println("Student Status: " + student.getStudentStatus().toString());
	}
}
