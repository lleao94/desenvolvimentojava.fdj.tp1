package br.com.fdvtp1.repository;

public class  DataRepository {

	private String[] studentNames;
	private double[] scoreV1;
	private double[] scoreV2;
	
	public DataRepository(int size){
		this.studentNames = new String[size];
		this.scoreV1 = new double[size];
		this.scoreV2 = new double[size];
	}
	
	public void registerStudentName(String student) throws Exception{
		Boolean status = false;
		for (int i = 0; i < studentNames.length; i++) {
			if(studentNames[i] == null){
				studentNames[i] = student;
				status = true;
				break;
			}
		}
		if(status == false){
			throw new Exception("Not have space in the database."); 
		}
	}
	
	public int getIdByStudent(String nameStudent){
		int index = -1;
		
		for (int i=0; i<this.studentNames.length; i++) {
			if (studentNames[i].equals(nameStudent)) {
				index = i;
				break;
			}
		}
		return index;
	}
	
	public void setScorev1(int id, double score){
		this.scoreV1[id] = score;
	}
	
	public void setScorev2(int id, double score){
		this.scoreV2[id] = score;
	}
	
	public double getScorev1(int id){
		return this.scoreV1[id];
	}
	
	public double getScorev2(int id){
		return this.scoreV2[id];
	}

	public String getName(int id){
		return this.studentNames[id];
	}
	
	public String[] getStudentNames(){
		return this.studentNames;
	}	
}
