## FDJ.TP1

Trabalho de Fundamentos do Desenvolvimento Java do Bloco Desenvolvimento Java do curso de Engenharia da Computação.

# Programa simples para o controle acadêmico de uma instituição de ensino

## Teste de Performance 1

Prática das seguintes habilidades desenvolvidas:

* Instalar programas necessários para o ambiente de desenvolvimento Java no computador
* Realizar configurações extras no ambiente de desenvolvimento
* Conhecer a estrutura básica de um programa Java
* Compilar um programa
* Corrigir erros no código
* Executar um programa
* Implementar a estrutura de um programa básico em Java
* Implementar o uso de váriáveis
* Implementar o uso de estruturas condicionais
* Implementar o uso de estruturas de repetição
* Implementar o uso de vetores